//Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

//AJAX це технологія, яка покращує взаємодію користувача з веб-сторінкою. AJAX дозволяє взаємодіяти з веб-сторінкою дозволяючи завантажувати
//та відображати лише частину сторінкибез без необхідності повного її перезавантаження.
//AJAX дозволяє виконувати запити до сервера та отримувати дані у форматі JSON або XML.

const url = "https://ajax.test-danit.com/api/swapi/films";

const film = document.querySelector(".film");

function sendRequest(url) {
  return fetch(url)
    .then((res) => res.json())
    .then((list) => {
      console.log(list);

      const ul = document.createElement("ul");
      ul.classList.add("list");
      film.appendChild(ul);

      list.forEach((films) => {
        let li = document.createElement("li");
        li.classList.add("list-item");
        ul.appendChild(li);

        let episodeNumber = document.createElement("p");
        episodeNumber.classList.add("list-number");
        episodeNumber.textContent = films.episodeId;
        li.appendChild(episodeNumber);

        let name = document.createElement("h3");
        name.classList.add("list-name");
        name.textContent = films.name;
        li.appendChild(name);

        let openingCrawl = document.createElement("p");
        openingCrawl.classList.add("list-text");
        openingCrawl.textContent = films.openingCrawl;
        li.appendChild(openingCrawl);

        films.characters.forEach((character) => {
          fetch(character)
            .then((res) => res.json())
            .then((data) => {
              let characterName = document.createElement("span");
              characterName.textContent = data.name;
              characterName.classList.add("character-list");
              li.appendChild(characterName);
            })
            .catch((err) => console.err(err.message));
        });
      });
    })
    .catch((err) => console.err(err.message));
}

sendRequest(url);
